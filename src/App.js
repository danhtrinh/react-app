import React, { Component } from 'react';
import MyRouter from './routes'

import './App.css';

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

import myReducer from './reducers/index';

// -- redux-saga
import rootSaga from './sagas/rootSaga';
import createSagaMiddleware from 'redux-saga';

// -- Middleware
const sagaMiddleware = createSagaMiddleware();
const store = createStore(myReducer, applyMiddleware(sagaMiddleware));

//import Example from './components/Example';
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MyRouter />
      </Provider>
    );
  }
}

sagaMiddleware.run(rootSaga);

export default App;
