import React, { Component } from 'react';

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import CreateItem from './pages/Item/CreateItem';
import CreateItemTwo from './pages/Item/CreateItemTwo';
import Layout from './pages/layout'
import Login from './pages/login'
import Home from './pages/home'

import createBrowserHistory from "history/createBrowserHistory";
const customHistory = createBrowserHistory();


export default class Routes extends Component {
	constructor(props) {
		super(props)
		this.state = {

		}
	}
	componentWillMount() {
		customHistory.push('/home')
	}

	render() {
		return (
			<Router history={customHistory} >
				<Switch>
					<Route path='/login' component={Login} />
					<Layout>
						<Route exact path='/home' component={Home} />
						<Route exact path='/users/create/two' component={CreateItemTwo} />
						<Route exact path='/users/create' component={CreateItem} />
					</Layout>
				</Switch>
			</Router>
		)
	}
}