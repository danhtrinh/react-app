import axios from 'axios';
import MovieModel from './../models/movieModel'

const getMoviesFromApi = () => {
    return  axios({
        method: 'get',
        url: 'https://facebook.github.io/react-native/movies.json',
        headers: {
            'Content-Type': 'application/json',
            // 'Authorization' : 'Bearer '+ localStorage.getItem('_token')
        }
    }).then(function (response) {
        const movieModel = new MovieModel()
        const data = movieModel.getMovie(response.data)
        return data
    }).catch(function (error) {
        console.log(error)
    })
}

export const Api = {
    getMoviesFromApi
};