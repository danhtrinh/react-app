import * as types from './../constants/ActionTypes' 

var initialState = {
    success: true,
    failed: false,
    authorName: ''
}

var myReducer = (state = initialState, action) => {
    switch(action.type) {
        case types.FETCH_MOVIE: 
            return state
        case types.FETCH_SUCCESSED:
            return { ...state, authorName: action.authorName }
        case types.FETCH_FAILED:
            return { success: false, failed: true, authorName: '' }
        default: 
            return state
    }
}

export default myReducer