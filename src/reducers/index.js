import { combineReducers } from 'redux';
import movie from './movieReducer'

const myReducer = combineReducers({
    movie
});

export default myReducer