import React, { Component } from 'react';
import { Link } from "react-router-dom";

import { connect } from 'react-redux';
import * as MovieActions from '../../actions/movieAction'

class Layout extends Component {
    constructor(props) {
        super(props)
        this.state = {
            
        }
    }

    render() {
        return (
            <div className="layout-container">
                <header>
                    This is header
                </header>
                <aside>
                    { this.props.children }
                </aside> 
                <footer>
                    This is footer
                </footer>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    // sidebar: state.sidebar
});

const mapDispatchToProps = (dispatch) => ({
    // onFetchMovie: () => { dispatch(MovieActions.fetchMovie()) }
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout)