import React, { Component } from 'react'
import { Link } from "react-router-dom"

// ==== component material ui
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'

// ==== redux
import { connect } from 'react-redux'
import * as MovieActions from '../../actions/movieAction'

import './login.css'

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            remember: false,
            chckRemember: true
        }
    }

    onHandleChange = (event) => {
        var target = event.target
        var name = target.name
        var value = target.type === 'checkbox' ? target.checked : target.value
        this.setState({
            [name]: value
        })
    }

    render() {
        return (
            <div className="login-container">

                <Card className="card-box">
                    <div className="card-header no-select">
                        <p>LOGIN</p>
                    </div>
                    <CardContent className="card-content">
                        <TextField
                            id="outlined-name"
                            label="Username"
                            className="username"
                            margin="normal"
                            variant="outlined"
                        />
                        <TextField
                            id="outlined-password"
                            label="Password"
                            type="password"
                            className="password"
                            margin="normal"
                            variant="outlined"
                        />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="chckRemember"
                                    checked={this.state.chckRemember}
                                    onChange={this.onHandleChange}
                                    style={{ color: '#ffc700' }}
                                />
                            }
                            label="Remember me"
                            className="no-select"
                        />


                    </CardContent>
                    <CardActions>
                        <Button
                            variant="contained"
                            color="primary"
                            style={styles.btnLogin}
                            className="btn-login"
                        >
                            <Link to="/home" style={styles.txtLogin}>LOGIN</Link>
                        </Button>
                    </CardActions>
                    <div style={{ textAlign: 'center' }}>
                        <p>
                            <span className="no-select">Not a member? &nbsp;</span>
                            <a href={'#'}
                                style={{ color: '#444', fontWeight: 'bold' }}
                                className="no-select"
                            >
                                Sign up now
                          </a>
                        </p>
                        <p>
                            <a href={'#'}
                                style={{ color: '#444', fontWeight: 'bold', textDecoration: 'none' }}
                                className="no-select"
                            >
                                Recover password
                          </a>
                        </p>
                    </div>
                </Card>

            </div>
        )
    }
}

const styles = {
    btnLogin: {
        margin: 'auto', 
        width: '93%', 
        backgroundColor: '#ffc700', 
        color: '#000', 
        borderRadius: 25, 
        height: 50
    },
    txtLogin: {
        color: '#000', 
        textDecoration: 'none', 
        width: '100%', 
        height: '100%'
    }
}

const mapStateToProps = state => ({
    sidebar: state.sidebar
});

const mapDispatchToProps = (dispatch) => ({
    onFetchMovie: () => { dispatch(MovieActions.fetchMovie()) }
});

export default connect(mapStateToProps, mapDispatchToProps)(Login)
