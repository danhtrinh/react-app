import React, { Component } from 'react';
import { Link } from "react-router-dom";

import { connect } from 'react-redux';
import * as MovieActions from '../../actions/movieAction'

import './home.css'

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            
        }
    }

    render() {
        return (
            <div className="home-container">
                {/* Code here */}
                <p>This is homepage</p>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    // sidebar: state.sidebar
});

const mapDispatchToProps = (dispatch) => ({
    // onFetchMovie: () => { dispatch(MovieActions.fetchMovie()) }
});

export default connect(mapStateToProps, mapDispatchToProps)(Home)