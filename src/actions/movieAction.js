import * as types from './../constants/ActionTypes' 

export const fetchMovie = () => {
    return {
        type: types.FETCH_MOVIE
    }
}

export const fetchSuccessed = (authorName) => {
    return {
        type: types.FETCH_SUCCESSED,
        authorName
    }
}

export const fetchFailed = () => {
    return {
        type: types.FETCH_FAILED
    }
}