import { takeLatest, all } from 'redux-saga/effects'

/* ------------- Types ------------- */

import * as types from './../constants/ActionTypes'

/* ------------- Sagas ------------- */

import { fetchMovies } from './movieSaga'

/* ------------- Connect Types To Sagas ------------- */

export default function * rootSaga () {
  yield all([
    // some sagas only receive an action
    takeLatest(types.FETCH_MOVIE, fetchMovies)
  ])
}
