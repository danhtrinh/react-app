import { put, call } from 'redux-saga/effects';
import { Api } from '../api/movieApi';

import * as types from './../constants/ActionTypes'

export function * fetchMovies() {
	try {
        const receivedMovie = yield call(Api.getMoviesFromApi)
        let authorName = receivedMovie.title
        console.log(authorName)
		yield put({ type: types.FETCH_SUCCESSED, authorName })
	} catch (error) {
		yield put({ type: types.FETCH_FAILED })
	}
}
